package ru.tsk.ilina.tm.service;

import ru.tsk.ilina.tm.api.repository.IProjectRepository;
import ru.tsk.ilina.tm.api.repository.ITaskRepository;
import ru.tsk.ilina.tm.api.service.IProjectTaskService;
import ru.tsk.ilina.tm.model.Project;
import ru.tsk.ilina.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;
    private final ITaskRepository taskRepository;

    public ProjectTaskService(IProjectRepository projectRepository, ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findTaskByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return taskRepository.findAllTaskByProjectId(projectId);
    }

    @Override
    public Task bindTaskById(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        if (projectRepository.existsById(projectId) && taskRepository.existsById(taskId))
            return taskRepository.bindTaskToProjectById(projectId, taskId);
        return null;
    }

    @Override
    public Task unbindTaskById(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        return taskRepository.unbindTaskToProjectById(projectId, taskId);
    }

    @Override
    public void removeAllTaskByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return;
        taskRepository.removeAllTaskByProjectId(projectId);
        projectRepository.removeByID(projectId);
    }

}
