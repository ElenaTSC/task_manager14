package ru.tsk.ilina.tm.controller;

import ru.tsk.ilina.tm.api.controller.IProjectTaskController;
import ru.tsk.ilina.tm.api.service.IProjectService;
import ru.tsk.ilina.tm.api.service.IProjectTaskService;
import ru.tsk.ilina.tm.api.service.ITaskService;
import ru.tsk.ilina.tm.model.Project;
import ru.tsk.ilina.tm.model.Task;
import ru.tsk.ilina.tm.util.TerminalUtil;

import java.util.List;

public class ProjectTaskController implements IProjectTaskController {

    private final ITaskService taskService;
    private final IProjectService projectService;
    private final IProjectTaskService projectTaskService;

    public ProjectTaskController(ITaskService taskService,
                                 IProjectService projectService,
                                 IProjectTaskService projectTaskService) {
        this.taskService = taskService;
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void findTaskByProjectId() {
        System.out.println("[ENTER PROJECT ID]");
        final String id = TerminalUtil.nextLine();
        final List<Task> tasks = projectTaskService.findTaskByProjectId(id);
        if (tasks == null) {
            System.out.println("[INCORRECT VALUES]");
            return;
        }
        for (Task task : tasks) System.out.println(task.toString());
    }

    @Override
    public void bindTaskToProjectById() {
        System.out.println("[ENTER PROJECT ID]");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.findByID(projectId);
        if (project == null) {
            System.out.println("[INCORRECT VALUES]");
            return;
        }
        System.out.println("[ENTER TASK ID]");
        final String taskId = TerminalUtil.nextLine();
        final Task task = taskService.findByID(taskId);
        if (task == null) {
            System.out.println("[INCORRECT VALUES]");
            return;
        }
        final Task taskUpdate = projectTaskService.bindTaskById(projectId, taskId);
        if (taskUpdate == null) System.out.println("[INCORRECT VALUES]");
    }

    @Override
    public void unbindTaskToProjectById() {
        System.out.println("[ENTER PROJECT ID]");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.findByID(projectId);
        if (project == null) {
            System.out.println("[INCORRECT VALUES]");
            return;
        }
        System.out.println("[ENTER TASK ID]");
        final String taskId = TerminalUtil.nextLine();
        final Task task = taskService.findByID(taskId);
        if (task == null) {
            System.out.println("[INCORRECT VALUES]");
            return;
        }
        final Task taskUpdate = projectTaskService.unbindTaskById(projectId, taskId);
        if (taskUpdate == null) System.out.println("[INCORRECT VALUES]");
    }

    @Override
    public void removeAllTaskByProjectId() {
        System.out.println("[ENTER PROJECT ID]");
        final String projectId = TerminalUtil.nextLine();
        projectTaskService.removeAllTaskByProjectId(projectId);
    }

}
