package ru.tsk.ilina.tm.component;

import ru.tsk.ilina.tm.api.controller.ICommandController;
import ru.tsk.ilina.tm.api.controller.IProjectController;
import ru.tsk.ilina.tm.api.controller.IProjectTaskController;
import ru.tsk.ilina.tm.api.controller.ITaskController;
import ru.tsk.ilina.tm.api.service.ICommandService;
import ru.tsk.ilina.tm.api.service.IProjectService;
import ru.tsk.ilina.tm.api.service.IProjectTaskService;
import ru.tsk.ilina.tm.api.service.ITaskService;
import ru.tsk.ilina.tm.constant.ArgumentConst;
import ru.tsk.ilina.tm.constant.TerminalConst;
import ru.tsk.ilina.tm.controller.CommandController;
import ru.tsk.ilina.tm.controller.ProjectController;
import ru.tsk.ilina.tm.controller.ProjectTaskController;
import ru.tsk.ilina.tm.controller.TaskController;
import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.model.Project;
import ru.tsk.ilina.tm.model.Task;
import ru.tsk.ilina.tm.repository.CommandRepository;
import ru.tsk.ilina.tm.repository.ProjectRepository;
import ru.tsk.ilina.tm.repository.TaskRepository;
import ru.tsk.ilina.tm.service.CommandService;
import ru.tsk.ilina.tm.service.ProjectService;
import ru.tsk.ilina.tm.service.ProjectTaskService;
import ru.tsk.ilina.tm.service.TaskService;

import java.util.Scanner;

public class Bootstrap {

    private final CommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);
    private final ICommandController commandController = new CommandController(commandService);
    private final TaskRepository taskRepository = new TaskRepository();
    private final ITaskService taskService = new TaskService(taskRepository);
    private final ITaskController taskController = new TaskController(taskService);
    private final ProjectRepository projectRepository = new ProjectRepository();
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    private final IProjectTaskController projectTaskController = new ProjectTaskController(taskService, projectService, projectTaskService);
    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    public void parseArgs(String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }

    public void parseArg(String arg) {
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            default:
                commandController.showErrorCommand();
        }
    }

    public void parseCommand(final String command) {
        switch (command) {
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.EXIT:
                commandController.exit();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.TASK_SHOW_BY_ID:
                taskController.findByID();
                break;
            case TerminalConst.TASK_SHOW_BY_INDEX:
                taskController.findByIndex();
                break;
            case TerminalConst.TASK_SHOW_BY_NAME:
                taskController.findByName();
                break;
            case TerminalConst.TASK_REMOVE_BY_ID:
                taskController.removeByID();
                break;
            case TerminalConst.TASK_REMOVE_BY_INDEX:
                taskController.removeByIndex();
                break;
            case TerminalConst.TASK_REMOVE_BY_NAME:
                taskController.removeByName();
                break;
            case TerminalConst.TASK_UPDATE_BY_ID:
                taskController.updateById();
                break;
            case TerminalConst.TASK_UPDATE_BY_INDEX:
                taskController.updateByIndex();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.PROJECT_SHOW_BY_ID:
                projectController.findByID();
                break;
            case TerminalConst.PROJECT_SHOW_BY_INDEX:
                projectController.findByIndex();
                break;
            case TerminalConst.PROJECT_SHOW_BY_NAME:
                projectController.findByName();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_ID:
                projectController.removeByID();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeByIndex();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_NAME:
                projectController.removeByName();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_ID:
                projectController.updateById();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateByIndex();
                break;
            case TerminalConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.PROJECT_START_BY_ID:
                projectController.startById();
                break;
            case TerminalConst.PROJECT_START_BY_INDEX:
                projectController.startByIndex();
                break;
            case TerminalConst.PROJECT_START_BY_NAME:
                projectController.startByName();
                break;
            case TerminalConst.PROJECT_FINISH_BY_ID:
                projectController.finishById();
                break;
            case TerminalConst.PROJECT_FINISH_BY_INDEX:
                projectController.finishByIndex();
                break;
            case TerminalConst.PROJECT_FINISH_BY_NAME:
                projectController.finishByName();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeStatusById();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeStatusByIndex();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_NAME:
                projectController.changeStatusByName();
                break;
            case TerminalConst.TASK_START_BY_ID:
                taskController.startById();
                break;
            case TerminalConst.TASK_START_BY_INDEX:
                taskController.startByIndex();
                break;
            case TerminalConst.TASK_START_BY_NAME:
                taskController.startByName();
                break;
            case TerminalConst.TASK_FINISH_BY_ID:
                taskController.finishById();
                break;
            case TerminalConst.TASK_FINISH_BY_INDEX:
                taskController.finishByIndex();
                break;
            case TerminalConst.TASK_FINISH_BY_NAME:
                taskController.finishByName();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_ID:
                taskController.changeStatusById();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeStatusByIndex();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_NAME:
                taskController.changeStatusByName();
                break;
            case TerminalConst.PROJECT_TASK_BIND:
                projectTaskController.bindTaskToProjectById();
                break;
            case TerminalConst.PROJECT_TASK_UNBIND:
                projectTaskController.unbindTaskToProjectById();
                break;
            case TerminalConst.PROJECT_TASK_REMOVE_BY_ID:
                projectTaskController.removeAllTaskByProjectId();
                break;
        }
    }

    public void start(String[] args) {
        System.out.println("**WELCOME TO TASK MANAGER**");
        parseArgs(args);
        initData();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND: ");
            String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    private void initData() {
        Project project1 = new Project("Project C", "-");
        project1.setStatus(Status.COMPLETED);
        projectService.add(project1);
        Project project2 = new Project("Project A", "-");
        project2.setStatus(Status.IN_PROGRESS);
        projectService.add(project2);
        Project project3 = new Project("Project B", "-");
        projectService.add(project3);
        Task task1 = new Task("Task B", "-");
        task1.setStatus(Status.COMPLETED);
        taskService.add(task1);
        Task task2 = new Task("Task A", "-");
        task2.setStatus(Status.COMPLETED);
        taskService.add(task2);
        Task task3 = new Task("Task C", "-");
        task3.setStatus(Status.COMPLETED);
        taskService.add(task3);
    }

}
